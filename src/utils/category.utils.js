import * as _ from 'lodash';

export const SortCategories = (categories) => {
  let filterNonCategories = _.filter(categories, (category) => {
    return (category.id === '0') || (category.id === '-1')
  });

  let filterCategories = _.difference(categories, filterNonCategories);
  let alphabetizedCategories = _.sortBy(filterCategories, (category) => {
    return category.name;
  });

  return [...filterNonCategories, ...alphabetizedCategories]
}

export const RemoveCategoryFromList = (category_id, categories) => {
  let alphabetizedCategories = SortCategories(categories);
  let removeCategoryIndex = _.findIndex(alphabetizedCategories, {id: category_id});
  alphabetizedCategories.splice(removeCategoryIndex, 1);
  return alphabetizedCategories;
}
