import * as _ from 'lodash';

export const ViewRecipesByCategory = (recipes, category) => {
  let viewableRecipes;
  if(category === '-1') {
    viewableRecipes = recipes;
  } else {
      let recipeByCategories = _.filter(recipes, (recipe) => {
        return recipe.category === category;
      });
      viewableRecipes = recipeByCategories;
  }
  return viewableRecipes.reduce(reduceRecipes, []);
}

export const UrlFriendly = (recipe_name) => {
  return "Blah"
}

function reduceRecipes(rows, key, index) {
  return (index % 8 == 0 ? rows.push([key])
      : rows[rows.length-1].push(key)) && rows;
}
