import { combineReducers } from 'redux';

import headerReducer from './header/header.reducer';
import categoryReducer from './category/category.reducer';
import recipeReducer from './recipe/recipe.reducer';
import userReducer from './user/user.reducer';
import recipeImageReducer from './recipe-image/recipe-image.reducer';
import errorCategoryReducer from './error-category/error-category.reducer';
import appReducer from './app/app.reducer';



const rootReducer = combineReducers({
  header: headerReducer,
  recipes: recipeReducer,
  recipeImages: recipeImageReducer,
  categories: categoryReducer,
  user: userReducer,
  errorCategory: errorCategoryReducer,
  app: appReducer
});

export default rootReducer;
