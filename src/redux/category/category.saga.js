import { takeLatest, call, put, all } from 'redux-saga/effects';
import {
  fetchCategoriesSuccess,
  fetchCategoriesFailure,
  removeCategorySuccess,
  removeCategoryFailure
} from './category.actions';
import { CategoryActionTypes } from './category.types';
import CategoryService from '../../services/categories.service';

export function* fetchCategoriesAsync() {
  try {
    const categories = yield call(CategoryService.List)
    yield put(fetchCategoriesSuccess(categories))
  } catch (error) {
    yield put(fetchCategoriesFailure(error))
  }
}

export function* removeCategory({ payload: id }) {
  try {
    const removeCategory = yield call(CategoryService.Remove, id);
    const categories = yield call(CategoryService.List)
    yield put(removeCategorySuccess(removeCategory))
    yield put(fetchCategoriesSuccess(categories))
  } catch (error) {
    yield put(removeCategoryFailure(error));
  }
}

export function* onFetchCategories() {
  yield takeLatest(CategoryActionTypes.FETCH_CATEGORIES_START, fetchCategoriesAsync);
}

export function* onRemoveCategory() {
  yield takeLatest(CategoryActionTypes.REMOVE_CATEGORY_START, removeCategory);
}

export function* categoriesSaga() {
  yield all([
    call(onFetchCategories),
    call(onRemoveCategory)
  ]);
}
