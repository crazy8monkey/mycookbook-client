import { createSelector } from 'reselect';

const categoryReducer = state => state.categories;

export const selectCategories = createSelector(
  [categoryReducer],
  state => state.list
);

export const selectCategoriesError = createSelector(
  [categoryReducer],
  state => state.error
);

export const addCategoriesError = createSelector(
  [categoryReducer],
  state => state.error
);

export const updateCategoriesError = createSelector(
  [categoryReducer],
  state => state.error
);

export const removeCategoriesError = createSelector(
  [categoryReducer],
  state => state.error
);
