import { CategoryActionTypes } from './category.types';

const INITIAL_STATE = {
  list: [],
  isLoading: false,
  error: null
};

const categoryReducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case CategoryActionTypes.FETCH_CATEGORIES_START:
      return {
        ...state,
        isLoading: true
      };
    case CategoryActionTypes.FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        list: payload
      };
    case CategoryActionTypes.ADD_NEW_CATEGORY_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null
      };
    case CategoryActionTypes.UPDATE_CATEGORY_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null
      };
    case CategoryActionTypes.REMOVE_CATEGORY_SUCCESS:
      return {
        ...state,
        isLoading: false
      };
    case CategoryActionTypes.ADD_NEW_CATEGORY_FAILURE:
    case CategoryActionTypes.FETCH_CATEGORIES_FAILURE:
    case CategoryActionTypes.UPDATE_CATEGORY_FAILURE:
    case CategoryActionTypes.REMOVE_CATEGORY_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload
      };
    case CategoryActionTypes.SUBMIT_CATETORY_FORM:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    default:
      return state;
  }
};

export default categoryReducer;
