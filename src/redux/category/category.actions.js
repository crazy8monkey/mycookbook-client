import { CategoryActionTypes } from './category.types';
import { createAction } from '../utils';

//grabbing the list
export const fetchCategoriesStart = () => createAction(CategoryActionTypes.FETCH_CATEGORIES_START);
export const fetchCategoriesSuccess = (categories) => createAction(CategoryActionTypes.FETCH_CATEGORIES_SUCCESS, categories);
export const fetchCategoriesFailure = (error) => createAction(CategoryActionTypes.FETCH_CATEGORIES_FAILURE, error);
//removing category
export const removeCategoryStart = (id) => createAction(CategoryActionTypes.REMOVE_CATEGORY_START, id);
export const removeCategorySuccess = (id) => createAction(CategoryActionTypes.REMOVE_CATEGORY_SUCCESS, id);
export const removeCategoryFailure = (error) => createAction(CategoryActionTypes.REMOVE_CATEGORY_FAILURE, error)
