import { UserActionTypes } from './user.types';
import { createAction } from '../utils';

export const setUser = (user) => createAction(
  UserActionTypes.SET_USER,
  user
);
