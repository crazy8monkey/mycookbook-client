import { createSelector } from 'reselect';

const userReducer = state => state.user;

export const selectUser = createSelector(
  [userReducer],
  state => state.current_user
);
