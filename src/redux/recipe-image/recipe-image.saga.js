import { takeLatest, call, put, all } from 'redux-saga/effects';
import {
  fetchRecipeImagesSuccess,
  fetchRecipeImagesError,
} from './recipe-image.actions';

import { RecipeImageActionTypes } from './recipe-image.types';
import RecipeImagesService from '../../services/recipe-images.service';

export function* fetchRecipeImagesAsync({ payload: id }) {
  try {
    const recipes = yield call(RecipeImagesService.List, id)
    yield put(fetchRecipeImagesSuccess(recipes))
  } catch (error) {
    yield put(fetchRecipeImagesError(error))
  }
}


export function* onFetchRecipeImages() {
  yield takeLatest(RecipeImageActionTypes.FETCH_RECIPE_IMAGES_START, fetchRecipeImagesAsync);
}

export function* recipeImagesSaga() {
  yield all([
    call(onFetchRecipeImages)
  ]);
}
