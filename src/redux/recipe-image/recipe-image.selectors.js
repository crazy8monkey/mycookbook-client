import { createSelector } from 'reselect';

const recipeImageReducer = state => state.recipeImages;

export const selectRecipeImages = createSelector(
  [recipeImageReducer],
  images => images.list
);


export const selectImageAddError = createSelector(
  [recipeImageReducer],
  images => images.error
);
