import { RecipeImageActionTypes } from './recipe-image.types';
import { createAction } from '../utils';

//grabbing  list
export const fetchRecipeImagesStart = (id) => createAction(RecipeImageActionTypes.FETCH_RECIPE_IMAGES_START, id);
export const fetchRecipeImagesSuccess = (images) => createAction(RecipeImageActionTypes.FETCH_RECIPE_IMAGES_SUCCESS, images);
export const fetchRecipeImagesError = (error) => createAction(RecipeImageActionTypes.FETCH_RECIPE_IMAGES_FAILURE, error);
