export const RecipeImageActionTypes = {
  //grabbing list
  FETCH_RECIPE_IMAGES_START: 'recipe_image/FETCH_RECIPE_IMAGES_START',
  FETCH_RECIPE_IMAGES_SUCCESS: 'recipe_image/FETCH_RECIPE_IMAGES_SUCCESS',
  FETCH_RECIPE_IMAGES_FAILURE: 'recipe_image/FETCH_RECIPE_IMAGES_FAILURE',
  //adding list
  ADD_RECIPE_IMAGES_START: 'recipe_image/ADD_RECIPE_IMAGES_START',
  ADD_RECIPE_IMAGES_SUCCESS: 'recipe_image/ADD_RECIPE_IMAGES_SUCCESS',
  ADD_RECIPE_IMAGES_FAILURE: 'recipe_image/ADD_RECIPE_IMAGES_FAILURE',
};
