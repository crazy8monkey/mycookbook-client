import { RecipeImageActionTypes } from './recipe-image.types';

const INITIAL_STATE = {
  list: [],
  isLoading: false,
  error: null,
};

const recipeImageReducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case RecipeImageActionTypes.FETCH_RECIPE_IMAGES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        list: payload
      };
    case RecipeImageActionTypes.ADD_RECIPE_IMAGES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        list: payload
      };
    case RecipeImageActionTypes.FETCH_RECIPE_IMAGES_FAILURE:
    case RecipeImageActionTypes.ADD_RECIPE_IMAGES_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload
      };
    default:
      return state;
  }
};

export default recipeImageReducer;
