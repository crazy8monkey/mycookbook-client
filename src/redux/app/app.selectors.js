import { createSelector } from 'reselect';

const appReducer = state => state.app;

export const selectLoadingSpinnerState = createSelector(
  [appReducer],
  app => app.showLoadingSpinner
);
