import { AppActionTypes } from './app.types';
import { createAction } from '../utils';

export const setLoadingSpinner = (show) => createAction(AppActionTypes.SHOW_LOADING_SPINNER, show);
