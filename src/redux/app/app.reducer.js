import { AppActionTypes } from './app.types';

const INITIAL_STATE = {
  showLoadingSpinner: false
};

const appReducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case AppActionTypes.SHOW_LOADING_SPINNER:
      return {
        showLoadingSpinner: payload
      };
    default:
      return state;
  }
};

export default appReducer;
