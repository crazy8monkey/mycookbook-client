export const ErrorCategoryActionTypes = {
  //grabbing list
  FETCH_CATEGORIES_START: 'error_category/FETCH_CATEGORIES_START',
  FETCH_CATEGORIES_SUCCESS: 'error_category/FETCH_CATEGORIES_SUCCESS',
  FETCH_CATEGORIES_FAILURE: 'error_category/FETCH_CATEGORIES_FAILURE',
};
