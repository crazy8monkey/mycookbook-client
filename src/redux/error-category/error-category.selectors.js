import { createSelector } from 'reselect';

const errorCategoryReducer = state => state.errorCategory;

export const selectList = createSelector(
  [errorCategoryReducer],
  state => state.list
);
