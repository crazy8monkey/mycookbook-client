import { ErrorCategoryActionTypes } from './error-category.types';
import { createAction } from '../utils';

//grabbing the list
export const fetchCategoriesStart = () => createAction(ErrorCategoryActionTypes.FETCH_CATEGORIES_START);
export const fetchCategoriesSuccess = (categories) => createAction(ErrorCategoryActionTypes.FETCH_CATEGORIES_SUCCESS, categories);
export const fetchCategoriesFailure = (error) => createAction(ErrorCategoryActionTypes.FETCH_CATEGORIES_FAILURE, error);
