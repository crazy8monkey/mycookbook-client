import { ErrorCategoryActionTypes } from './error-category.types';

const INITIAL_STATE = {
  list: [],
  isLoading: false,
  error: null
};

const errorCategoryReducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case ErrorCategoryActionTypes.FETCH_CATEGORIES_START:
      return {
        ...state,
        isLoading: true
      };
    case ErrorCategoryActionTypes.FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        list: payload
      };
    case ErrorCategoryActionTypes.FETCH_CATEGORIES_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload
      };
    default:
      return state;
  }
}

export default errorCategoryReducer;
