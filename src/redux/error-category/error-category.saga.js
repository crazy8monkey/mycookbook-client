import { takeLatest, call, put, all } from 'redux-saga/effects';
import {
  fetchCategoriesSuccess,
  fetchCategoriesFailure,
} from './error-category.actions';
import { ErrorCategoryActionTypes } from './error-category.types';
import ErrorCategoryService from '../../services/error-categories.service';

export function* fetchCategoriesAsync() {
  try {
    let initCategories = [{ id: '0', name: "Uncategorized" }]
    const categories = yield call(ErrorCategoryService.List);
    initCategories.push(...categories);
    yield put(fetchCategoriesSuccess(initCategories))
  } catch (error) {
    yield put(fetchCategoriesFailure(error))
  }
}

export function* onFetchCategories() {
  yield takeLatest(ErrorCategoryActionTypes.FETCH_CATEGORIES_START, fetchCategoriesAsync);
}

export function* errorCategoriesSaga() {
  yield all([
    call(onFetchCategories),
  ]);
}
