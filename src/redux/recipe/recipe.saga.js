import { takeLatest, call, put, all } from 'redux-saga/effects';
import {
  fetchRecipesSuccess,
  fetchRecipesFailure,
  fetchRecipeViewSuccess,
  fetchRecipeViewFailure,
  fetchRecipeRemoveSuccess,
  fetchRecipeRemoveFailure,
  updateRecipeSuccess,
  updateRecipeFailure,
} from './recipe.actions';

import { RecipeActionTypes } from './recipe.types';
import RecipeService from '../../services/recipes.service';

export function* fetchRecipesAsync() {
  try {
    const recipes = yield call(RecipeService.List)
    yield put(fetchRecipesSuccess(recipes))
  } catch (error) {
    yield put(fetchRecipesFailure(error))
  }
}

export function* fetchRecipeViewAsync({ payload: id }) {
  try {
    const recipe = yield call(RecipeService.Get, id)
    yield put(fetchRecipeViewSuccess(recipe))
  } catch (error) {
    yield put(fetchRecipeViewFailure(error))
  }
}

export function* fetchRecipeRemoveAsync({ payload: id }) {
  try {
    const remove = yield call(RecipeService.Remove, id)
    const recipes = yield call(RecipeService.List)

    yield put(fetchRecipeRemoveSuccess(remove))
    yield put(fetchRecipesSuccess(recipes))
  } catch (error) {
    yield put(fetchRecipeRemoveFailure(error))
  }
}

export function* updateRecipeAsync({ payload: {id, recipe} }) {
  try {
    const update = yield call(RecipeService.Update, id, recipe)
    const recipes = yield call(RecipeService.List);

    yield put(updateRecipeSuccess(update));
    yield put(fetchRecipesSuccess(recipes));
  } catch (error) {
    yield put(updateRecipeFailure(error))
  }
}

export function* onFetchRecipes() {
  yield takeLatest(RecipeActionTypes.FETCH_RECIPES_START, fetchRecipesAsync);
}

export function* onFetchRecipeView() {
  yield takeLatest(RecipeActionTypes.FETCH_RECIPE_VIEW_START, fetchRecipeViewAsync);
}

export function* onFetchRecipeRemove() {
  yield takeLatest(RecipeActionTypes.FETCH_RECIPE_REMOVE_START, fetchRecipeRemoveAsync);
}

export function* onUpdateRecipe() {
  yield takeLatest(RecipeActionTypes.UPDATE_RECIPE_START, updateRecipeAsync);
}

export function* recipesSaga() {
  yield all([
    call(onFetchRecipes),
    call(onFetchRecipeView),
    call(onFetchRecipeRemove),
    call(onUpdateRecipe)
  ]);
}
