import { createSelector } from 'reselect';

const recipeReducer = state => state.recipes;

export const selectRecipes = createSelector(
  [recipeReducer],
  recipes => recipes.list
);

export const selectRecipeView = createSelector(
  [recipeReducer],
  recipes => recipes.view
);

export const selectView = createSelector(
  [recipeReducer],
  recipes => recipes.view
);

export const selectListError = createSelector(
  [recipeReducer],
  recipes => recipes.error
);

export const selectError = createSelector(
  [recipeReducer],
  recipes => recipes.error
);

export const selectLoading = createSelector(
  [recipeReducer],
  recipes => recipes.isLoading
);
