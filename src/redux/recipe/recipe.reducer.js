import { RecipeActionTypes } from './recipe.types';


const INITIAL_STATE = {
  list: [],
  isLoading: false,
  error: null,
  view: {
    id:'',
    name:'',
    category:'',
    image:null,
    ingredients:'',
    instructions:''
  }
};

const recipeReducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case RecipeActionTypes.FETCH_RECIPES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        list: payload
      };
    case RecipeActionTypes.UPDATE_RECIPE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
      };
    case RecipeActionTypes.FETCH_RECIPE_VIEW_SUCCESS:
      return {
        ...state,
        isLoading: false,
        view: payload
      };
    case RecipeActionTypes.FETCH_RECIPE_REMOVE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        view: {
          id:'',
          name: '',
          image:null,
          category: '',
          ingredients: '',
          instructions:''
        }
      };
    case RecipeActionTypes.FETCH_RECIPES_FAILURE:
    case RecipeActionTypes.FETCH_RECIPE_VIEW_FAILURE:
    case RecipeActionTypes.FETCH_RECIPE_REMOVE_FAILURE:
    case RecipeActionTypes.UPDATE_RECIPE_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload
      };
    default:
      return state;
  }
};

export default recipeReducer;
