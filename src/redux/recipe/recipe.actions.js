import { RecipeActionTypes } from './recipe.types';
import { createAction } from '../utils';

export const submitRecipeForm = () => createAction(RecipeActionTypes.SUBMIT_RECIPE_FORM);
//grabbing  list
export const fetchRecipesStart = () => createAction(RecipeActionTypes.FETCH_RECIPES_START);
export const fetchRecipesSuccess = (recipes) => createAction(RecipeActionTypes.FETCH_RECIPES_SUCCESS, recipes);
export const fetchRecipesFailure = (error) => createAction(RecipeActionTypes.FETCH_RECIPES_FAILURE, error);
//adding new recipe
export const addRecipe = (recipe, images) => createAction(RecipeActionTypes.ADD_NEW_RECIPE, {recipe, images});
export const addRecipeResult = (payload) => createAction(RecipeActionTypes.ADD_NEW_RECIPE_RESULT, payload);
//grabbing recipe
export const fetchRecipeViewStart = (id) => createAction(RecipeActionTypes.FETCH_RECIPE_VIEW_START, id);
export const fetchRecipeViewSuccess = (id) => createAction(RecipeActionTypes.FETCH_RECIPE_VIEW_SUCCESS, id);
export const fetchRecipeViewFailure = (error) => createAction(RecipeActionTypes.FETCH_RECIPE_VIEW_FAILURE, error);
//removing recipe
export const fetchRecipeRemoveStart = (id) => createAction(RecipeActionTypes.FETCH_RECIPE_REMOVE_START, id);
export const fetchRecipeRemoveSuccess = (id) => createAction(RecipeActionTypes.FETCH_RECIPE_REMOVE_SUCCESS, id);
export const fetchRecipeRemoveFailure = (error) => createAction(RecipeActionTypes.FETCH_RECIPE_REMOVE_FAILURE, error);
//updating recipe
export const updateRecipeStart = (id, recipe) => createAction(RecipeActionTypes.UPDATE_RECIPE_START, {id, recipe});
export const updateRecipeSuccess = (recipe) => createAction(RecipeActionTypes.UPDATE_RECIPE_SUCCESS, recipe);
export const updateRecipeFailure = (error) => createAction(RecipeActionTypes.UPDATE_RECIPE_FAILURE, error);
