import { createSelector } from 'reselect';

const currentHeader = state => state.header;

export const selectDropDownState = createSelector(
  [currentHeader],
  header => header.dropdownMenu
);
