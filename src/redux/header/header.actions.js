import { HeaderActionTypes } from './header.types';

export const toggleDropDown = () => ({
    type: HeaderActionTypes.TOGGLE_DROPDOWN
});

export const resetDropDown = () => ({
    type: HeaderActionTypes.RESET_DROPDOWN
});
