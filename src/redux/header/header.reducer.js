import { HeaderActionTypes } from './header.types';

const INITIAL_STATE = {
  dropdownMenu: false
};

const headerReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HeaderActionTypes.TOGGLE_DROPDOWN:
      return {
        dropdownMenu: !state.dropdownMenu
      };
    case HeaderActionTypes.RESET_DROPDOWN:
      return {
        dropdownMenu: false
      };
    default:
      return state;
  }
};

export default headerReducer;
