import { all, call } from 'redux-saga/effects';

import { categoriesSaga } from './category/category.saga';
import { recipesSaga } from './recipe/recipe.saga';
import { recipeImagesSaga } from './recipe-image/recipe-image.saga';
import { errorCategoriesSaga } from './error-category/error-category.saga';

export function* rootSaga() {
  yield all([
    call(categoriesSaga),
    call(recipesSaga),
    call(recipeImagesSaga),
    call(errorCategoriesSaga)
  ]);
}
