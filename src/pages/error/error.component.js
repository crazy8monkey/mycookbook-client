//react
import React from 'react';
//styles
import './error.component.styles.scss';
//services
import ErrorService from '../../services/error.service';
//components
import FormInput from '../../components/form-input/form-input.component';
import CustomButtonComponent from '../../components/custom-button/custom-button.component';
import ReactQuillEditor from '../../components/quill-editor/quill-editor.component';
//redux
import { fetchCategoriesStart } from '../../redux/error-category/error-category.actions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
//misc
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Form, Field } from 'react-final-form';

class Error extends React.Component {

  componentDidMount() {
    const { fetchCategories } = this.props;
    fetchCategories();
  }

  submitForm = async e => {
    return new Promise(resolve => {
      ErrorService.Add(e).then((response) => {
        resolve(true);
        toast.success("Your error has been submitted", { position: "bottom-center" });
      }).catch(error => {
        resolve(error.response.data);
      });
    });
  }

  render() {
    let loginCheck = localStorage.getItem('token');
    let initValues = {
      category: "0",
      description: ''
    }

    if(!loginCheck) {
      initValues = Object.assign({first_name: '', last_name: ''},  initValues)
    }

    return(
      <React.Fragment>
        <div className="headerPush"></div>
        <div className='container-fluid'>
          <div className="row">
            <div className="col">
              <div className="errorSubmissionForm contentWrapper">
                <Form
                  onSubmit={this.submitForm}
                  initialValues={initValues}
                  render={({ handleSubmit, form }) => (
                    <form onSubmit={handleSubmit}>
                      <h1>Submit an error/suggestion</h1>
                      {!loginCheck &&
                        <div className="section">
                          <div className="row">
                            <div className="col">
                              <Field name="first_name">
                                {props => (
                                  <FormInput
                                    label="First Name"
                                    type="text"
                                    name={props.input.name}
                                    handleChange={props.input.onChange}
                                    value={props.input.value}
                                    errors={props.meta.submitError}
                                  />
                                )}
                              </Field>
                            </div>
                            <div className="col">
                              <Field name="last_name">
                                {props => (
                                  <FormInput
                                    label="Last Name"
                                    type="text"
                                    name={props.input.name}
                                    handleChange={props.input.onChange}
                                    value={props.input.value}
                                    errors={props.meta.submitError}
                                  />
                                )}
                              </Field>
                            </div>
                          </div>
                        </div>
                      }
                      <div className="section">
                        <div className="label">
                          Type in the description of the particular error you have encounters, or an improvement you would like to see
                        </div>

                        <Field name="description">
                          {props => (
                            <React.Fragment>
                              <ReactQuillEditor
                                value={props.input.value}
                                onChange={props.input.onChange}
                              />
                              {props.meta.submitError?.map((error) => (
                                <div className="errorText" key={error}>{error}</div>
                              ))}
                            </React.Fragment>
                          )}
                        </Field>
                      </div>

                      <div className="section">
                        <CustomButtonComponent type="blueButton" value="SAVE" buttonType="submit" />
                      </div>

                    </form>
                  )}
                />
              </div>
            </div>
          </div>
        </div>
        <ToastContainer />
      </React.Fragment>
    )
  }
}


const mapDispatch = dispatch => ({
  fetchCategories:() => dispatch(fetchCategoriesStart())
});

export default connect(null, mapDispatch)(Error);

//export default Error;
