//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './login.component.styles.scss';
//services
import LoginService from '../../services/login.service';
import TokenService from '../../services/token.service';
//redux
import { connect } from 'react-redux';
import { setUser } from '../../redux/user/user.actions';
import { setLoadingSpinner } from '../../redux/app/app.actions';
//components
import FormInput from '../../components/form-input/form-input.component';
import CustomButtonComponent from '../../components/custom-button/custom-button.component';
import { Form, Field } from 'react-final-form';
import { FORM_ERROR } from 'final-form'

class Login extends React.Component {

  submitForm = async e => {
    const { history, setLoadingSpinner, setUser } = this.props;
    setLoadingSpinner(true);

    e.role = "User";
    return new Promise(resolve => {
      LoginService.Login(e).then(response => {
        resolve(true);
        TokenService.setAccessToken(response.token);
        TokenService.setRefreshToken(response.refresh_token);
        setUser(response.user);
        setLoadingSpinner(false);
        history.push("/recipes");
      }).catch(error => {
        setLoadingSpinner(false);
        const errors = error.response.data;
        if(errors.form) {
          resolve({ [FORM_ERROR]: errors.form });
        } else {
          resolve(errors);
        }
      });
    });
  }

  render() {
    return(
      <React.Fragment>
        <div className="pageHeader">
          <div className="content">
            Login
          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <div className="loginForm">
                <Form
                  onSubmit={this.submitForm}
                  initialValues={{
                    email: '',
                    password: '',
                  }}
                  render={({ handleSubmit, submitError }) => (
                    <form onSubmit={handleSubmit}>
                      {submitError &&
                        <div className="row">
                          <div className="col">
                            <div className="errorText">{submitError}</div>
                          </div>
                        </div>
                      }
                      <div className="row">
                        <div className="col">
                          <Field name="email">
                            {props => (
                              <FormInput
                                label="Email"
                                type="email"
                                name={props.input.name}
                                handleChange={props.input.onChange}
                                value={props.input.value}
                                errors={props.meta.submitError}
                              />
                            )}
                          </Field>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col">
                          <Field name="password">
                            {props => (
                              <FormInput
                                label="Password"
                                type="password"
                                name={props.input.name}
                                handleChange={props.input.onChange}
                                value={props.input.value}
                                errors={props.meta.submitError}
                              />
                            )}
                          </Field>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col">
                          <CustomButtonComponent type="blueButton" value="LOGIN" buttonType="submit" />
                          <span style={{marginLeft: '10px'}}>New To MyCookBook?</span> <Link to="/signup">Sign Up Now</Link>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col" style={{ borderTop: '1px solid #f7f7f7', marginTop: '20px', paddingTop: '5px'}}>
                          <Link to="/forgot-credentials">Forgot Credentials?</Link>
                        </div>
                      </div>
                    </form>
                  )}
                />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

const mapDispatch = dispatch => ({
  setUser:(user) => dispatch(setUser(user)),
  setLoadingSpinner:(show) => dispatch(setLoadingSpinner(show))
});

export default connect(null, mapDispatch)(Login);
