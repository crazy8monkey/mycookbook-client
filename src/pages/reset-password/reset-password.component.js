//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './reset-password.component.styles.scss';
//services
import ProfileService from '../../services/profile.service';
//components
import FormInput from '../../components/form-input/form-input.component';
import CustomButtonComponent from '../../components/custom-button/custom-button.component';
//misc
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Form, Field } from 'react-final-form';

class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: '',
      expired_status: false
    }
  }

  componentDidMount() {
    if(localStorage.getItem('user_reset_password')) {
      let userResetRequest = JSON.parse(localStorage.getItem('user_reset_password'))
      this.setState({ user_id: userResetRequest.user_id });
      let currentDate = new Date();
      let expiredDate = new Date(userResetRequest.reset_password)
      if(currentDate > expiredDate) {
        localStorage.clearItem('user_reset_password')
        this.setState({expired_status : true})
      }
    } else {
      this.setState({expired_status : true})
    }
  }

  submitForm = async e => {
    const { user_id } = this.state;
    const { history } = this.props;

    return new Promise(resolve => {
      e.user_id = user_id;
      ProfileService.NewPassword(e).then((response) => {
        localStorage.clearItem('user_reset_password')
        toast.success("Password Updated", { position: "bottom-center" });
        setTimeout(function(){
          resolve(true);
          history.push(`/login`);
        }, 3000);
      }).catch(error => {
        resolve(error.response.data);
      });
    });

  }

  resetForm = () => {
    return (
      <div className="row">
        <div className="col">
          <div className="resetPasswordForm">
            <div className="text">
              Please type in your new password
             </div>
             <Form
               onSubmit={this.submitForm}
               initialValues={{
                 password: '',
                 retype_password: '',
               }}
               render={({ handleSubmit }) => (
                 <form onSubmit={handleSubmit}>
                  <div>
                    <Field name="password">
                      {props => (
                        <FormInput
                          label="Password"
                          type="password"
                          name={props.input.name}
                          handleChange={props.input.onChange}
                          value={props.input.value}
                          errors={props.meta.submitError}
                        />
                      )}
                    </Field>
                  </div>
                  <div>
                    <Field name="retype_password">
                      {props => (
                        <FormInput
                          label="Retype Password"
                          type="password"
                          name={props.input.name}
                          handleChange={props.input.onChange}
                          value={props.input.value}
                          errors={props.meta.submitError}
                        />
                      )}
                    </Field>
                  </div>
                  <div>
                    <CustomButtonComponent type="blueButton" value="SAVE" buttonType="submit" />
                  </div>
                 </form>
               )}
             />
          </div>
        </div>
      </div>
    )
  }

  expiredPage = () => {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col">
            <div className="resetPasswordForm">
              This page has been expired, please go the following link below and start again
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="resetPasswordForm">
              <Link to="/forgot-credentials">Reset your password</Link>
            </div>
          </div>
        </div>
      </React.Fragment>

    )
  }

  render() {
    const { expired_status } = this.state;
    return (
      <React.Fragment>
        <div className="pageHeader">
          <div className="content">
            Reset your Credentials
          </div>
        </div>
        {expired_status &&
          this.expiredPage()
        }
        {!expired_status &&
          this.resetForm()
        }
        <ToastContainer />
      </React.Fragment>
    );
  }
}

export default ResetPassword;
