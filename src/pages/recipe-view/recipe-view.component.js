//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './recipe-view.component.styles.scss';
//redux
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  fetchRecipeViewStart,
  fetchRecipeRemoveStart
} from '../../redux/recipe/recipe.actions';
import {
  selectRecipeView
} from '../../redux/recipe/recipe.selectors';
import { fetchRecipeImagesStart } from '../../redux/recipe-image/recipe-image.actions';
import { selectRecipeImages } from '../../redux/recipe-image/recipe-image.selectors';
//misc
import { toast } from 'react-toastify';


class RecipeView extends React.Component {

  componentDidMount() {
    const { recipe_id } = this.props.match.params;
    const { cathImages, fetchRecipe } = this.props;
    fetchRecipe(recipe_id);
    cathImages(recipe_id)
  }

  editRecipe = () => {
    const { history } = this.props;
    const { recipe_id } = this.props.match.params;
    history.push(`${recipe_id}/edit`);
  }

  removeRecipe = () => {
    const { history, removeRecipe } = this.props;
    const { recipe_id } = this.props.match.params;
    try {
      removeRecipe(recipe_id);
      toast.dark("Recipe Removed", { position: "bottom-center" });
      history.push(`/recipes`);
    } catch (error) {
      console.log("recipe remove error => ", error);
    }
  }

  render() {
    const { selectRecipeView, selectRecipeImages } = this.props;
    const { name, ingredients, instructions } = selectRecipeView;

    return(
      <React.Fragment>
        <div className="headerPush"></div>
        <div className='container-fluid'>
          <div className="row">
            <div className="col">
              <div className="breadCrumb contentWrapper">
                <Link to="/recipes">Recipes</Link> / Viewing Recipe
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <div className="viewingRecipeContaner contentWrapper">
                <div className="recipeInfo">
                  <div className="edit" onClick={this.editRecipe}>
                    <i className="fas fa-pencil-alt"></i>
                  </div>
                  <div className="remove" onClick={this.removeRecipe}>
                    <i className="fas fa-trash"></i>
                  </div>
                  <h1>{name}</h1>
                  <div className="images" style={{ backgroundImage: `url(data:${selectRecipeImages[0]?.picture_type};base64,${selectRecipeImages[0]?.picture})` }}>
                  </div>
                  <div className="section">
                    <strong>Ingredients</strong>
                    <div dangerouslySetInnerHTML={{__html: ingredients }} />
                  </div>
                  <div className="mobileAddSpace">
                    Add Space will go here
                  </div>
                  <div className="section">
                    <strong>Instructions</strong>
                    <div dangerouslySetInnerHTML={{__html: instructions }} />
                  </div>
                </div>
                <div className="addSpace">
                  Add will go here
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  selectRecipeView: selectRecipeView,
  selectRecipeImages: selectRecipeImages
});

const mapDispatch = dispatch => ({

  //setRecipeView:(recipe) => dispatch(setRecipeView(recipe)),
  cathImages:(id) => dispatch(fetchRecipeImagesStart(id)),
  fetchRecipe:(id) => dispatch(fetchRecipeViewStart(id)),
  removeRecipe:(id) => dispatch(fetchRecipeRemoveStart(id))
});

export default connect(mapStateToProps, mapDispatch)(RecipeView);
