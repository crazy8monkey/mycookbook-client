//react
import React from 'react';
//styles
import './home.component.styles.scss';

class Home extends React.Component {

  render() {
    return(
      <React.Fragment>
        <div className="entry-text-container">
          <div className="entry-text-link-section">
            <div className="welcomText">
              <div><h1>The last cook book</h1></div>
              <div><h2>you will ever need.</h2></div>
            </div>
            <div className="recipe-description">
        			Take your handwritten recipe and store it online.
        			Get rid of the hassle of losing that great recipe, and have it stored in one location!
        		</div>
          </div>
        </div>
        <div className="backgroundImage"></div>
      </React.Fragment>
    )
  }
}

export default Home;
