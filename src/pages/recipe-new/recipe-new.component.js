//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './recipe-new.component.styles.scss';
//services
import RecipeService from '../../services/recipes.service';
import RecipeImagesService from '../../services/recipe-images.service';
//components
import RecipeForm from '../../components/recipe-form/recipe-form.component';
//redux
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  selectLoading
} from '../../redux/recipe/recipe.selectors';
import {
  selectImageAddError
} from '../../redux/recipe-image/recipe-image.selectors';
import { setLoadingSpinner } from '../../redux/app/app.actions';
//misc
import { toast } from 'react-toastify';

class RecipeNew extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      recipe_images: []
    };
  }

  onImageChange = async images => {
    this.setState({ recipe_images:images });
  }

  submitNewRecipe = async values => {
    const { history, setLoadingSpinner } = this.props;
    const { recipe_images } = this.state;
    setLoadingSpinner(true);

    return new Promise(resolve => {
      setLoadingSpinner(false);
      RecipeService.Add(values).then((recipe) => {
        if(recipe_images.length > 0) {
          RecipeImagesService.Add(recipe.id, recipe_images).then((response) => {
            resolve(true);
          }).catch(error => {
            //this needs to be worked on
            //resolve({ [FORM_ERROR]: errors.form });
          });
        } else {
          resolve(true);
        }

        toast.success("Recipe Added", { position: "bottom-center" })
        history.push(`/recipes`);

      }).catch(error => {
        resolve(error.response.data);
      });
    });
  }

  render() {
    return (
      <React.Fragment>
        <div className="headerPush"></div>
        <div className='container-fluid'>
          <div className="row">
            <div className="col">
              <div className="breadCrumb contentWrapper">
                <Link to="/recipes">Recipes</Link> / Add New Recipe
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <RecipeForm
                type="New"
                images={[]}
                initialFormValues={{
                  name: '',
                  category: '0',
                  ingredients: '',
                  instructions: ''
                }}
                onImagesChange={this.onImageChange}
                onSubmit={this.submitNewRecipe}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  selectLoading: selectLoading,
  imagesUploadError: selectImageAddError
});

const mapDispatch = dispatch => ({
  setLoadingSpinner:(show) => dispatch(setLoadingSpinner(show))
});

export default connect(mapStateToProps, mapDispatch)(RecipeNew);
