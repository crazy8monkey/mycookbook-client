//react
import React from 'react';
//styles
import './recipes.component.styles.scss'
//components
import RecipeCard from '../../components/recipe-card/recipe-card.component';
import CategorySelect from '../../components/category-select/category-select.component';
//redux
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  fetchRecipesStart,
  fetchRecipeViewStart
} from '../../redux/recipe/recipe.actions';
import {
  selectRecipes
} from '../../redux/recipe/recipe.selectors';
//misc
import { ViewRecipesByCategory } from '../../utils/recipe.utils';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Recipes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: "-1",
      loading: true
    }
  }

  componentDidMount() {
    const { fetchRecipes } = this.props;
    fetchRecipes();
  }

  viewRecipe = (recipe_id, event) => {
    const { history, fetchRecipe } = this.props;
    fetchRecipe(recipe_id);
    history.push(`recipes/${recipe_id}`);
  }

  catchCurrentCategory = (e) => {
    this.setState({ category: e });
  }

  render() {
    const { category } = this.state;
    let viewableRecipes = ViewRecipesByCategory(this.props.recipes, category);
    console.log("viewableRecipes => ", viewableRecipes);

    return(
      <React.Fragment>
        <div className="headerPush"></div>
        <div className="mainPage">
          <div className="recipeList">
            {viewableRecipes.length > 0 &&
              <React.Fragment>
                {viewableRecipes.map(recipes => (
                  <React.Fragment>
                    <div className="row">
                      {recipes.map(recipe => (
                        <div className="view-recipe" key={recipe.id}>
                          <RecipeCard
                            recipe={recipe}
                            viewOnClick={this.viewRecipe}
                          />
                        </div>
                      ))}
                    </div>
                    {recipes.length === 8 &&
                      <div className="AddSpace">
                        Add Space Goes Here
                      </div>
                    }
                  </React.Fragment>
                ))}
              </React.Fragment>
            }
            {viewableRecipes.length === 0 &&
              <div className="emptyRecipeList">
                There are no recipes to show
              </div>
            }
          </div>
          <div className="categories">
            <div className="content">
              <CategorySelect selectedCategory={this.catchCurrentCategory} />
            </div>
          </div>
        </div>
        <ToastContainer />
      </React.Fragment>

    )
  }
}


const mapStateToProps = createStructuredSelector({
  recipes: selectRecipes
});

const mapDispatch = dispatch => ({
  fetchRecipes:() => dispatch(fetchRecipesStart()),
  fetchRecipe:(id) => dispatch(fetchRecipeViewStart(id))
});

export default connect(mapStateToProps, mapDispatch)(Recipes);
