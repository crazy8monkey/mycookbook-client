//react
import React from 'react';
//styles
import './profile.component.styles.scss';
//services
import ProfileService from '../../services/profile.service';
//component
import FormInput from '../../components/form-input/form-input.component';
import CustomButtonComponent from '../../components/custom-button/custom-button.component';
import DragAndDrop from '../../components/drag-drop/drag-drop.component';
//redux
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectUser } from '../../redux/user/user.selectors';
import { setUser } from '../../redux/user/user.actions';
import { setLoadingSpinner } from '../../redux/app/app.actions';
//misc
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Form, Field } from 'react-final-form';

class Profile extends React.Component {

  componentDidMount() {
    const { setUser } = this.props;
    ProfileService.Get().then((response) => {
      setUser(response);
    });
  }

  submitForm = async e => {
    console.log(e);
    const { selectUser, setUser, setLoadingSpinner } = this.props;
    setLoadingSpinner(true);
    return new Promise(resolve => {
      setLoadingSpinner(false);
      ProfileService.Update(e).then(response => {
        resolve(true);
        const currentUser = selectUser
        currentUser.first_name = e.first_name;
        currentUser.last_name = e.last_name;
        currentUser.email = e.email;
        setUser(currentUser);
        toast.success("Profile Saved", { position: "bottom-center" })
      }).catch(error => {
        resolve(error.response.data);
      });
    });
  }

  handleDrop = (event) => {
    const { selectUser, setUser } = this.props;
    ProfileService.NewProfilePic(event[0]).then((response) => {
      const currentUser = selectUser
      currentUser.profile = response.profile_pic;
      currentUser.profile_type = response.profile_pic_type;
      setUser(currentUser);
      this.forceUpdate();
      toast.success("Profile Picture Uploaded", { position: "bottom-center" })
    });
  }

  render() {
    const { selectUser } = this.props;
    const { first_name, last_name, email, profile_type, profile } = selectUser;
    const initialValues = {
      first_name,
      last_name,
      email,
      new_password: '',
      confirm_password: '',
    }

    return(
      <React.Fragment>
        <div className="headerPush"></div>
        <div className='container-fluid'>
          <div className="row">
            <div className="col">
              <div className="contentWrapper">
                <div className="profileContainer">
                  <div className="profilePic">
                    <div className="image" style={{ backgroundImage: `url(data:${profile_type};base64,${profile})` }}></div>
                    <DragAndDrop
                      maxFiles={1}
                      refresh={true}
                      dropFiles={this.handleDrop}
                      currentImages={[]}
                      />
                  </div>
                  <div className="profileContent">
                    <Form
                      onSubmit={this.submitForm}
                      initialValues={initialValues}
                      render={({ handleSubmit }) => (
                        <form onSubmit={handleSubmit}>
                          <h1>Your Profile</h1>
                          <div className="section">
                            <div className="row">
                              <div className="col">
                                <Field name="first_name">
                                  {props => (
                                    <FormInput
                                      label="First Name"
                                      type="text"
                                      name={props.input.name}
                                      handleChange={props.input.onChange}
                                      value={props.input.value}
                                      errors={props.meta.submitError}
                                    />
                                  )}
                                </Field>
                              </div>
                              <div className="col">
                                <Field name="last_name">
                                  {props => (
                                    <FormInput
                                      label="Last Name"
                                      type="text"
                                      name={props.input.name}
                                      handleChange={props.input.onChange}
                                      value={props.input.value}
                                      errors={props.meta.submitError}
                                    />
                                  )}
                                </Field>
                              </div>
                            </div>
                          </div>
                          <div className="section">
                            <div className="row">
                              <div className="col">
                                <Field name="email">
                                  {props => (
                                    <FormInput
                                      label="Email"
                                      type="email"
                                      name={props.input.name}
                                      handleChange={props.input.onChange}
                                      value={props.input.value}
                                      errors={props.meta.submitError}
                                    />
                                  )}
                                </Field>
                              </div>
                            </div>
                          </div>
                          <div className="section">
                            <div className="row">
                              <div className="col">
                                <Field name="new_password">
                                  {props => (
                                    <FormInput
                                      label="Password"
                                      type="password"
                                      name={props.input.name}
                                      handleChange={props.input.onChange}
                                      value={props.input.value}
                                      errors={props.meta.submitError}
                                    />
                                  )}
                                </Field>
                              </div>
                              <div className="col">
                                <Field name="confirm_password">
                                  {props => (
                                    <FormInput
                                      label="Retype Password"
                                      type="password"
                                      name={props.input.name}
                                      handleChange={props.input.onChange}
                                      value={props.input.value}
                                      errors={props.meta.submitError}
                                    />
                                  )}
                                </Field>
                              </div>
                            </div>
                          </div>
                          <div className="section" style={{ borderTop: '1px solid #efeeee'}}>
                            <CustomButtonComponent type="blueButton" value="SAVE" buttonType="submit" />
                          </div>
                        </form>
                      )}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ToastContainer />
      </React.Fragment>
    )
  }
}


const mapStateToProps = createStructuredSelector({
  selectUser: selectUser
});

const mapDispatch = dispatch => ({
  setUser:(user) => dispatch(setUser(user)),
  setLoadingSpinner:(show) => dispatch(setLoadingSpinner(show))
});

export default connect(mapStateToProps, mapDispatch)(Profile);
