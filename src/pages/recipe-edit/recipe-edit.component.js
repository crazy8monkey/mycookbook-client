//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './recipe-edit.component.styles.scss'
//services
import RecipeService from '../../services/recipes.service';
import RecipeImagesService from '../../services/recipe-images.service';
import ImageService from '../../services/image.service';
//components
import RecipeForm from '../../components/recipe-form/recipe-form.component';
//redux
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { updateRecipeStart } from '../../redux/recipe/recipe.actions';
import { selectRecipeView } from '../../redux/recipe/recipe.selectors';
import { selectRecipeImages } from '../../redux/recipe-image/recipe-image.selectors';
import { fetchRecipeImagesStart } from '../../redux/recipe-image/recipe-image.actions';
import { setLoadingSpinner } from '../../redux/app/app.actions';
//misc
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class RecipeEdit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      recipe_images: []
    };
  }

  componentDidMount() {
    const { selectRecipeImages } = this.props;
    ImageService.InitRecipeImages(selectRecipeImages).then(response => {
      this.setState({ recipe_images:response });
    });
  }

  submitUpdateRecipe = async values => {
    const { recipe_id } = this.props.match.params;
    const { recipe_images } = this.state;
    const { setLoadingSpinner } = this.props;
    setLoadingSpinner(true);

    return new Promise(resolve => {
      setLoadingSpinner(false);
      RecipeService.Update(recipe_id, values).then((recipe) => {
        if(recipe_images.length > 0) {
          RecipeImagesService.Update(recipe_id, recipe_images).then((response) => {
            resolve(true);
          }).catch(error => {
            //this needs to be worked on
            //resolve({ [FORM_ERROR]: errors.form });
          });
        } else {
          resolve(true);
        }

        toast.success("Recipe Updated", { position: "bottom-center" });

      }).catch(error => {
        resolve(error.response.data);
      });
    });
  }

  onImageChange = async images => {
    this.setState({ recipe_images:images });
  }

  render() {
    const { selectRecipeView, selectRecipeImages } = this.props;

    return(
      <React.Fragment>
        <div className="headerPush"></div>
        <div className='container-fluid'>
          <div className="row">
            <div className="col">
              <div className="breadCrumb contentWrapper">
                <Link to="/recipes">Recipes</Link> / <Link to={`/recipes/${selectRecipeView.id}`}>Viewing Recipe</Link> /
                Edit Recipe
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <RecipeForm
                type="Edit"
                images={selectRecipeImages}
                initialFormValues={{
                  name: selectRecipeView.name,
                  category: selectRecipeView.category,
                  ingredients: selectRecipeView.ingredients,
                  instructions: selectRecipeView.instructions
                }}
                onImagesChange={this.onImageChange}
                onSubmit={this.submitUpdateRecipe}
              />
            </div>
          </div>
        </div>
        <ToastContainer />
      </React.Fragment>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  selectRecipeView: selectRecipeView,
  selectRecipeImages: selectRecipeImages
});

const mapDispatch = dispatch => ({
  catchImages:(id) => dispatch(fetchRecipeImagesStart(id)),
  updateRecipe:(id, recipe) => dispatch(updateRecipeStart(id, recipe)),
  setLoadingSpinner:(show) => dispatch(setLoadingSpinner(show))
});

export default connect(mapStateToProps, mapDispatch)(RecipeEdit);
