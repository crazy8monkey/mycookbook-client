//react
import React from 'react';
//styles
import './forgot-credentials.component.styles.scss';
//services
import PasswordService from '../../services/password.service';
//redux
import { connect } from 'react-redux';
import { setLoadingSpinner } from '../../redux/app/app.actions';
//components
import FormInput from '../../components/form-input/form-input.component';
import CustomButtonComponent from '../../components/custom-button/custom-button.component';
import { Form, Field } from 'react-final-form';

class ForgotCredentials extends React.Component {

  submitForm = async e => {
    const { history, setLoadingSpinner } = this.props;
    setLoadingSpinner(true);

    return new Promise(resolve => {
      PasswordService.Reset(e).then((response) => {
        resolve(true);
        setLoadingSpinner(false);
        localStorage.setItem('user_reset_password', JSON.stringify(response));
        history.push("/reset-password");
      }).catch(error => {
        setLoadingSpinner(false);
        resolve(error.response.data);
      });
    });
  }

  render() {
    return (
      <React.Fragment>
        <div className="pageHeader">
          <div className="content">
            Retrieve Credentials
          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col">
            <Form
              onSubmit={this.submitForm}
              initialValues={{
                email: ''
              }}
              render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit}>
                  <div className="content credentialsForm">
                    <div className="retrieveCredientialsText">
                      Please enter your email in the inbox below.<br />
                      If your email is valid/registered into our system,<br />
                      Your email credentials will be emailed to you.
                    </div>
                    <Field name="email">
                      {props => (
                        <FormInput
                          label="Your Email"
                          type="email"
                          name={props.input.name}
                          handleChange={props.input.onChange}
                          value={props.input.value}
                          errors={props.meta.submitError}
                        />
                      )}
                    </Field>
                    <div>
                      <CustomButtonComponent type="blueButton" value="SUBMIT" buttonType="submit" />
                    </div>
                  </div>
                </form>
              )}
            />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapDispatch = dispatch => ({
  setLoadingSpinner:(show) => dispatch(setLoadingSpinner(show))
});

export default connect(null, mapDispatch)(ForgotCredentials);
