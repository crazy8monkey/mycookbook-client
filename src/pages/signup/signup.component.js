//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './signup.component.styles.scss';
//services
import SignupService from '../../services/signup.service';
import TokenService from '../../services/token.service';
//redux
import { connect } from 'react-redux';
import { setUser } from '../../redux/user/user.actions';
import { setLoadingSpinner } from '../../redux/app/app.actions';
//components
import FormInput from '../../components/form-input/form-input.component';
import CustomButtonComponent from '../../components/custom-button/custom-button.component';
//misc
import { Form, Field } from 'react-final-form';

class Signup extends React.Component {

  submitForm = async e => {
    const { history, setLoadingSpinner, setUser } = this.props;
    setLoadingSpinner(true);

    e.user_role = "User";
    return new Promise(resolve => {
      SignupService.Signup(e).then((response) => {
        resolve(true);
        TokenService.setAccessToken(response.token);
        TokenService.setRefreshToken(response.refresh_token);
        setLoadingSpinner(false);
        setUser(response.user);
        history.push(`/recipes`);
      }).catch(error => {
        resolve(error.response.data);
        setLoadingSpinner(false);
      });
    });
  }

  render() {
    return(
      <React.Fragment>
        <div className="pageHeader">
          <div className="content">
            Sign Up
          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <Form
                onSubmit={this.submitForm}
                initialValues={{
                  first_name: '',
                  last_name: '',
                  email: '',
                  new_password: '',
                  confirm_password: '',
                }}
                render={({ handleSubmit }) => (
                  <form onSubmit={handleSubmit}>
                    <div className="signupForm">
                      <div className="row">
                        <div className="col">
                          <Field name="first_name">
                            {props => (
                              <FormInput
                                label="First Name"
                                type="text"
                                name={props.input.name}
                                handleChange={props.input.onChange}
                                value={props.input.value}
                                errors={props.meta.submitError}
                              />
                            )}
                          </Field>
                        </div>
                        <div className="col">
                          <Field name="last_name">
                            {props => (
                              <FormInput
                                label="Last Name"
                                type="text"
                                name={props.input.name}
                                handleChange={props.input.onChange}
                                value={props.input.value}
                                errors={props.meta.submitError}
                              />
                            )}
                          </Field>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col">
                          <Field name="email">
                            {props => (
                              <FormInput
                                label="Email"
                                type="email"
                                name={props.input.name}
                                handleChange={props.input.onChange}
                                value={props.input.value}
                                errors={props.meta.submitError}
                              />
                            )}
                          </Field>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col">
                          <Field name="new_password">
                            {props => (
                              <FormInput
                                label="Password"
                                type="password"
                                name={props.input.name}
                                handleChange={props.input.onChange}
                                value={props.input.value}
                                errors={props.meta.submitError}
                              />
                            )}
                          </Field>
                        </div>
                        <div className="col">
                          <Field name="confirm_password">
                            {props => (
                              <FormInput
                                label="Confirm Password"
                                type="password"
                                name={props.input.name}
                                handleChange={props.input.onChange}
                                value={props.input.value}
                                errors={props.meta.submitError}
                              />
                            )}
                          </Field>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col formExtra">
                          <CustomButtonComponent type="blueButton" value="SAVE ACCOUNT" buttonType="submit" />
                          <span style={{marginLeft: '10px'}}>Have an account?</span> <Link to="/login">Login</Link>
                        </div>
                      </div>
                    </div>
                  </form>
                )}
              />
            </div>
          </div>
        </div>
      </React.Fragment>

    )
  }
}

const mapDispatch = dispatch => ({
  setUser:(user) => dispatch(setUser(user)),
  setLoadingSpinner:(show) => dispatch(setLoadingSpinner(show))
});

export default connect(null, mapDispatch)(Signup);
