const previewFiles = (files) => {
  return files.map(file => Object.assign(file, {
    preview: URL.createObjectURL(file)
  }))
}

class ImageService {
  InitRecipeImages(images) {
    const currentLoadedImages = images.map((file) => {
      let fileBuffer = `data:${file.picture_type};base64,${file.picture}`;
      return fetch(fileBuffer).then(res => {
        return res.arrayBuffer().then(buf => {
          return new File([buf], file.id, { type: file.picture_type });
        })
      })
    });

    return Promise.all(currentLoadedImages).then((results) => {
      return previewFiles(results)
    });
  }
}

export default new ImageService();
