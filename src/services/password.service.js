import api from './api.service';
const apiUrl = "/password";

class PasswordService {
  Reset(data) {
    return api.post(`${apiUrl}/reset`, data).then((resp) => resp.data);
  }
  New(data) {
    return api.put(`${apiUrl}/new`, data).then((resp) => resp.data);
  }
}

export default new PasswordService();
