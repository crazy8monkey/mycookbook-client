import api from './api.service';
const apiUrl = "/error-category";

class ErrorCategoryService {
  List() {
    return api.get(apiUrl).then((resp) => resp.data);
  }
}

export default new ErrorCategoryService();
