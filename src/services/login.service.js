import api from './api.service';
const apiUrl = "/login";

class LoginService {
  Login(data) {
    return api.post(apiUrl, data).then((resp) => resp.data);
  }
}

export default new LoginService();
