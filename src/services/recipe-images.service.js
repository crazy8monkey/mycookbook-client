import api from './api.service';
import axios from "axios"
const apiUrl = "/recipe-image";

class RecipeImagesService {
  List(recipe_id) {
    return api.get(`${apiUrl}/${recipe_id}`).then((resp) => resp.data);
  }
  Add(recipeId, images) {
    const recipeImages = new FormData();
    recipeImages.append("recipe", recipeId);
    images.forEach(image => recipeImages.append("imageFiles", image));

    return api.post(apiUrl, recipeImages).then((resp) => resp.data)
  }
  Update(recipeId, images) {
    const recipeImages = new FormData();
    recipeImages.append("recipe", recipeId);
    images.forEach(image => recipeImages.append("imageFiles", image));

    return api.put(apiUrl, recipeImages).then((resp) => resp.data)
  }
}

export default new RecipeImagesService();
