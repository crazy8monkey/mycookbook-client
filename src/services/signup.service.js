import api from './api.service';
const apiUrl = "/signup";

class SignupService {
  Signup(data) {
    return api.post(apiUrl, data).then((resp) => resp.data);
  }
}

export default new SignupService();
