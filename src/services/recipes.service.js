import api from './api.service';
const apiUrl = "/recipes";

class RecipeService {
  List() {
    return api.get(apiUrl).then((resp) => resp.data)
  }
  Add(data) {
    return api.post(apiUrl, data).then((resp) => resp.data)
  }
  Get(id) {
    return api.get(`${apiUrl}/${id}`).then((resp) => resp.data)
  }
  Update(id, data) {
    return api.put(`${apiUrl}/${id}`, data).then((resp) => resp.data)
  }
  Remove(id) {
    return api.delete(`${apiUrl}/${id}`).then((resp) => resp.data)
  }
}

export default new RecipeService();
