import api from './api.service';
const apiUrl = "/profile";

class ProfileService {
  Get() {
    return api.get(apiUrl).then((resp) => resp.data);
  }
  Update(data) {
    return api.put(apiUrl, data).then((resp) => resp.data);
  }
  NewProfilePic(data) {
    const recipeImageData = new FormData();
    recipeImageData.append("imageName", data.name);
    recipeImageData.append("imageFile", data);

    return api.post(`${apiUrl}/picture`, recipeImageData).then((resp) => resp.data);
  }
  Signup(data) {
    return api.post(apiUrl, data).then((resp) => resp.data);
  }
}

export default new ProfileService();
