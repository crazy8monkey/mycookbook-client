import api from './api.service';
const apiUrl = "/token";

class TokenService {
  getAccessToken() {
    return localStorage.getItem('token')
  }
  getRefreshToken() {
    return localStorage.getItem('refresh_token')
  }
  setAccessToken(token) {
    localStorage.setItem('token', token)
  }
  setRefreshToken(token) {
    localStorage.setItem('refresh_token', token)
  }
  removeAccessToken() {
    localStorage.removeItem("token");
  }
  removeRefreshToken() {
    localStorage.removeItem('refresh_token');
  }
  newRefreshToken() {
    return api.post(apiUrl, {
      refresh_token: localStorage.getItem('refresh_token')
    }).then((resp) => resp.data)
  }
}

export default new TokenService();



//
// export const NewTokens = (token) => {
//   return GET('api/token', token)
// }
