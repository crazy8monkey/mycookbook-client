import api from './api.service';
const apiUrl = "/error";

class ErrorService {
  Add(data) {
    return api.post(apiUrl, data).then((resp) => resp.data)
  }
}

export default new ErrorService();
