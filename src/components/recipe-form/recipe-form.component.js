//react
import React from 'react';
//styles
import './recipe-form.component.styles.scss';
//components
import CustomButtonComponent from '../custom-button/custom-button.component';
import FormInput from '../form-input/form-input.component';
import DragAndDrop from '../drag-drop/drag-drop.component';
import ReactQuillEditor from '../quill-editor/quill-editor.component';
//redux
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCategories } from '../../redux/category/category.selectors';
import { Form, Field } from 'react-final-form';
//misc
import * as _ from 'lodash';

class RecipeForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      recipe_images: props.images
    }
  }

  handleDrop = (event) => {
    this.setState({ recipe_images: event });
    this.props.onImagesChange(event);
  }


  render() {
    const { selectCategories, initialFormValues } =  this.props;
    const { recipe_images } = this.state;

    const selectableCategories = _.filter(selectCategories, (category) => {
      return category.id !== '-1'
    });
    return(
      <Form
        onSubmit={this.props.onSubmit}
        initialValues={initialFormValues}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <div className="recipeEditForm contentWrapper">
              <h1>{this.props.type} Recipe</h1>

              <div className="section">
                <Field name="name">
                  {props => (
                    <FormInput
                      label="Recipe Name"
                      type="text"
                      name={props.input.name}
                      handleChange={props.input.onChange}
                      value={props.input.value}
                      errors={props.meta.submitError}
                    />
                  )}
                </Field>
              </div>
              <div className="section">
                <DragAndDrop
                  maxFiles={1}
                  currentImages={recipe_images}
                  dropFiles={this.handleDrop}
                  />
              </div>
              <div className="section">
                Category
                <Field name="category">
                  {props => (
                    <select onChange={props.input.onChange} value={props.input.value}>
                      {selectableCategories.map((category) => (
                        <option key={category.id} value={category.id}>{category.name}</option>
                      ))}
                    </select>
                  )}
                </Field>
              </div>
              <div className="section">
                Ingredients
                <Field name="ingredients">
                  {props => (
                    <React.Fragment>
                      <ReactQuillEditor
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                      {props.meta.submitError?.map((error) => (
                        <div className="errorText" key={error}>{error}</div>
                      ))}
                    </React.Fragment>
                  )}
                </Field>
              </div>
              <div className="section">
                Instructions
                <Field name="instructions">
                  {props => (
                    <React.Fragment>
                      <ReactQuillEditor
                        value={props.input.value}
                        onChange={props.input.onChange}
                      />
                      {props.meta.submitError?.map((error) => (
                        <div className="errorText" key={error}>{error}</div>
                      ))}
                    </React.Fragment>
                  )}
                </Field>
              </div>

              <div className="section">
                <CustomButtonComponent type="blueButton" value="SAVE" buttonType="submit" />
              </div>
            </div>
          </form>
        )}
      />
    )
  }
}

const mapStateToProps = createStructuredSelector({
  selectCategories: selectCategories
});

export default connect(mapStateToProps, null)(RecipeForm);
