//react
import React from 'react';
//styles
import './form-input.component.styles.scss';

const displayErrors = (errors) => {
  if(errors && errors.length > 0) {
    return (
      <React.Fragment>
        {errors.map((error) => (
          <div className="errorText" key={error}>{error}</div>
        ))}
      </React.Fragment>
    );
  }
}

const FormInput = ({ handleChange, label, type, name, value, errors }) => (
  <div className="inputLine">
      {label}
      <input type={type} name={name} onChange={handleChange} value={value}/>
      {displayErrors(errors)}
  </div>
);

export default FormInput;
