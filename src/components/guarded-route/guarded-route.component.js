import React from 'react';
import { Route, Redirect } from "react-router-dom";

//https://github.com/forceofseth/route-guard/blob/master/src/App.js
const GuardedRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        localStorage.getItem("token")
            ? <Component {...props} />
            : <Redirect to='/' />
    )} />
)

export default GuardedRoute;
