//react
import React from 'react';
//styles
import './category-select.component.styles.scss';
//services
import CategoryService from '../../services/categories.service';
//components
import FormInput from '../form-input/form-input.component';
//redux
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  fetchCategoriesStart,
  removeCategoryStart,
} from '../../redux/category/category.actions';
import {
  selectCategories,
  removeCategoriesError
} from '../../redux/category/category.selectors';
import { setLoadingSpinner } from '../../redux/app/app.actions';
//misc
import * as _ from 'lodash';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Form, Field } from 'react-final-form';


class CategorySelect extends React.Component {
  constructor(props) {
    super(props);

    this.state =  {
      mode: '',
      selected: "0"
    }
  }

  componentDidMount() {
    const { fetchCategories } = this.props;
    fetchCategories();
  }

  categoryChange = e => {
    this.setState({ selected: e.target.value });
    this.props.selectedCategory(e.target.value);
  }

  changeCategoryMode = (mode) => {
    this.setState({ mode });
  }

  removeCategory = e => {
    const { selected } = this.state;
    const { removeCategory, removeCategoriesError } = this.props;
    if(removeCategoriesError == null) {
      removeCategory(selected);
      this.setState({ selected: "-1" });
      this.props.selectedCategory("-1");
      toast.dark("Category Removed", { position: "bottom-center" });
    } else {
      toast.error("Category Removed", { position: "bottom-center" });
    }
  }

  submitCategory = async values => {
    const { mode } = this.state;
    if(mode === "Add") {
      return this.saveCategory(values);
    }

    if(mode === "Edit") {
      return this.updateCategory(values);
    }
  }

  saveCategory(category) {
    const { fetchCategories, setLoadingSpinner } = this.props;
    setLoadingSpinner(true);

    return new Promise(resolve => {
      setLoadingSpinner(false);
      CategoryService.Add(category).then((response) => {
        resolve(true);
        this.changeCategoryMode('');
        toast.success("Category Added", { position: "bottom-center" });
        fetchCategories();
      }).catch(error => {
        resolve(error.response.data);
      });
    });
  }

  updateCategory(category) {
    const { selected } = this.state;
    const { fetchCategories, setLoadingSpinner } = this.props;
    setLoadingSpinner(true);

    return new Promise(resolve => {
      setLoadingSpinner(false);
      CategoryService.Update(selected, category).then((response) => {
        resolve(true);
        this.changeCategoryMode('');
        toast.success("Category Saved", { position: "bottom-center" });
        fetchCategories();
      }).catch(error => {
        resolve(error.response.data);
      });
    });
  }

  categoryForm = (mode) => {
    const initFormValues = { name: '' }
    const { selected } = this.state;
    const { selectCategories } = this.props;

    let categoryIndex = _.findIndex(selectCategories, {id: selected});
    if(mode === "Edit") {
      initFormValues.name = selectCategories[categoryIndex].name
    }

    return (
      <Form
        onSubmit={this.submitCategory}
        initialValues={initFormValues}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <div className="addEditCategoryForm">
              <div>
               <Field name="name">
                 {props => (
                   <FormInput
                     label={`${mode} Category`}
                     type="text"
                     name={props.input.name}
                     handleChange={props.input.onChange}
                     value={props.input.value}
                     errors={props.meta.submitError}
                   />
                 )}
               </Field>
              </div>
              <div className="btns">
                <button>
                  <i className="far fa-save"></i>
                </button>
                <i className="fas fa-times" onClick={() => this.changeCategoryMode('')}></i>
              </div>
            </div>
          </form>
        )}
      />
    )
  }

  categoryDropDown = (selected) => {
    const { selectCategories } = this.props;
    const editableCategory = (selected !== "-1") && (selected !== "0")
    const nonEditableCategory = (selected === "-1") || (selected === "0")
    return (
      <React.Fragment>
        <div>Viewing Category</div>
        <div className="categoryList">
          <select onChange={this.categoryChange} value={selected}>
            {selectCategories.map((category) => (
              <option key={category.id} value={category.id}>{category.name}</option>
            ))}
          </select>
          <div className="categoryBtns">
            {editableCategory &&
              <React.Fragment>
                <i className="fas fa-pencil-alt" onClick={() => this.changeCategoryMode('Edit')}></i>
                <i className="fas fa-trash" onClick={this.removeCategory}></i>
              </React.Fragment>
            }
            {nonEditableCategory &&
              <i className="fas fa-plus" onClick={() => this.changeCategoryMode('Add')}></i>
            }
          </div>
        </div>
      </React.Fragment>
    )
  }

  render() {
    const { selected, mode } = this.state;

    return(
      <React.Fragment>
        {mode === '' &&
          this.categoryDropDown(selected)
        }
        {mode !== '' &&
          this.categoryForm(mode)
        }
      </React.Fragment>
    )
  }
}


const mapStateToProps = createStructuredSelector({
  selectCategories: selectCategories,
  removeCategoriesError: removeCategoriesError
});

const mapDispatch = dispatch => ({
  fetchCategories:() => dispatch(fetchCategoriesStart()),
  removeCategory:(id) => dispatch(removeCategoryStart(id)),
  setLoadingSpinner:(show) => dispatch(setLoadingSpinner(show))
});

export default connect(mapStateToProps, mapDispatch)(CategorySelect);
