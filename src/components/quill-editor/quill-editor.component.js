//react
import React from 'react';
//misc
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const ReactQuillEditor = ({ value, onChange }) => (
  <ReactQuill
    value={value}
    onChange={onChange} 
  />
);

export default ReactQuillEditor;
