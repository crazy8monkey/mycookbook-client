//react
import React from 'react';
//styles
import './loading-spinner.component.style.scss';
//misc
import loadingSpinner from './spinner.gif';

const LoadingSpinner = () => {
  return (
    <div className="loadingSpinner">
      <img src={loadingSpinner} alt="Loading..." />
    </div>
  )
}

export default LoadingSpinner;
