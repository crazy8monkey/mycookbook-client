//react
import React from 'react';
import { Link } from 'react-router-dom';
//styles
import './custom-button.component.styles.scss';

const CustomButtonComponent = ({ type, value, buttonType, url, onClick }) => {
  if(buttonType === "submit") {
    return (
      <button type="submit" className={type} onClick={onClick}>
        {value}
      </button>
    )
  } else if(buttonType === "link") {
    return (
      <Link to={url} className={type}>
        {value}
      </Link>
    );
  } else {
    return null
  }
};

export default CustomButtonComponent;
