//https://medium.com/@650egor/simple-drag-and-drop-file-upload-in-react-2cb409d88929
//https://react-dropzone.js.org/

import React from 'react';
//misc
import * as _ from 'lodash';
import Dropzone from 'react-dropzone';

const style={
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out'
}

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box'
};

const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16
};

const thumbInner = {
  display: 'flex',
  minWidth: 0
};

const img = {
  display: 'block',
  width: 'auto',
  height: '100%'
};

const imgContainer = {
  position:'relative'
}

const imgContainerDelete = {
  position:'absolute',
  top: '-10px',
  left: '-10px',
  background: '#e5e5e5',
  borderRadius: '50%',
  padding:'2px 8px',
  color: '#8e8e8e',
  boxShadow:'2px 2px 5px #000'
}

class DragAndDrop extends React.Component {
  constructor(props) {
    super(props);

    this.onDrop = (files) => {
      props.dropFiles(files)
      if(props.refresh) {
        this.setState({files: []});
      } else {
        this.setState({files: this.previewFiles(files)})
      }
    };

    this.state = {
      files: []
    };
  }

  previewFiles = (files) => {
    return files.map(file => Object.assign(file, {
      preview: URL.createObjectURL(file)
    }))
  }

  deleteOnClick = event => {
    const { files } = this.state;
    const fileIndex = _.findIndex(files, {name: event.name});
    files.splice(fileIndex, 1);
    this.setState({ files: files})
    this.props.dropFiles(files)
  }

  componentDidMount() {
    const { currentImages } = this.props;
    const currentLoadedImages = currentImages.map((file) => {
      let fileBuffer = `data:${file.picture_type};base64,${file.picture}`;
      return fetch(fileBuffer).then(res => {
        return res.arrayBuffer().then(buf => {
          return new File([buf], file.id, { type: file.picture_type });
        })
      })
    });

    Promise.all(currentLoadedImages).then((results) => {
      this.setState({files: this.previewFiles(results)})
    });
  }

  render() {
    const { files } = this.state;

    const thumbs = files.map(file => (
      <div style={thumb} key={file.name}>
        <div style={thumbInner}>
          <div style={imgContainer}>
            <img src={file.preview} alt="recipe" style={img} />
            <div style={imgContainerDelete} onClick={() => this.deleteOnClick(file)}>
              <i className="fas fa-times"></i>
            </div>
          </div>
        </div>
      </div>
    ));

    return (
      <Dropzone
        maxFiles={this.props.maxFiles}
        accept={'image/*'}
        onDrop={this.onDrop}>
        {({getRootProps, getInputProps}) => (
          <React.Fragment>
            <div {...getRootProps({style})}>
              <input {...getInputProps()} />
              <p>Drag 'n' drop some files here, or click to select files</p>
            </div>
            <aside style={thumbsContainer}>
              {thumbs}
            </aside>
          </React.Fragment>
        )}
      </Dropzone>
    )
  }
}

export default DragAndDrop
