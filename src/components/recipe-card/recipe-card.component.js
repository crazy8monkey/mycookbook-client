//react
import React from 'react';
//styles
import './recipe-card.component.styles.scss';
//misc
import nopicture from './nopicture.png';

const mainImage = (image) => {
  return <div className="mainImage" style={{ backgroundImage: `url(data:${image?.type};base64,${image?.picture}` }}></div>
}

const RecipeCard = ({ recipe, viewOnClick }) => {
  return (
    <div className="recipeCard" onClick={(event) => viewOnClick(recipe.id, event)}>
      <div className="title" title={recipe.name}>
        {recipe.name}
      </div>
      {mainImage(recipe.image)}
    </div>
  )
}

export default RecipeCard;
