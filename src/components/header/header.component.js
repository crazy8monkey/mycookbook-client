//react
import React from 'react'
import { Link, Redirect } from 'react-router-dom';
//styles
import './header.component.style.scss';
//redux
import { connect } from 'react-redux';
import { selectDropDownState } from '../../redux/header/header.selectors';
import { toggleDropDown, resetDropDown } from '../../redux/header/header.actions';
import { createStructuredSelector } from 'reselect';
//misc
import homeLogo from './home-logo.png';

const logout = () => {
  localStorage.clear();
}

const dropDownLinks = (userLoggedIn, resetDropDown) => {
  if(userLoggedIn) {
    return(
      <React.Fragment>
        <Link className="mobileDropDownLink" to="/recipes/new" onClick={() => { resetDropDown(); }}>
          <div className="link">New Recipe</div>
        </Link>
        <Link to="/profile" onClick={() => { resetDropDown(); }}>
          <div className="link">Your Profile</div>
        </Link>
        <Link to="/error" onClick={() => { resetDropDown(); }}>
          <div className="link">Errors / Suggestions</div>
        </Link>
        <Link to="/" onClick={() => { logout(); }}>
          <div className="link">Logout</div>
        </Link>
      </React.Fragment>
    )
  } else {
    return(
      <React.Fragment>
        <Link to="/signup" onClick={() => { resetDropDown(); }}>
          <div className="link">Sign up</div>
        </Link>
        <Link to="/login" onClick={() => { resetDropDown(); }}>
          <div className="link">Login</div>
        </Link>
      </React.Fragment>
    )
  }
}

const menuButtons = (userLoggedIn) => {
  if(userLoggedIn) {
    return(
      <Link className="GreenButton mobileHide" to="/recipes/new">+ RECIPE</Link>
    )
  } else {
    return(
      <React.Fragment>
        <Link className="OrangeButton mobileHide" to="/signup">SIGN UP</Link>
        <Link className="OrangeButton mobileHide" to="/login">LOGIN</Link>
      </React.Fragment>
    )
  }
}


const HeaderComponent = ({ userLoggedIn, dropdownState, toggleDropDown, resetDropDown }) => {
  const headerClassName = userLoggedIn ? "appHeader" : "normal-header";
  return (
    <header className={headerClassName}>
      <div className="container-fluid">
        <div className="row">
          <div className='col'>
            <div className="content">
              <div className="logoImage">
                {userLoggedIn &&
                  <Link to="/recipes">
                    <img src={homeLogo} alt="MyCookBook" />
                  </Link>
                }
                {!userLoggedIn &&
                  <Link to="/">
                    <img src={homeLogo} alt="MyCookBook" />
                  </Link>
                }
              </div>
              <div className="buttons">
                {menuButtons(userLoggedIn)}
                <div className={`${userLoggedIn ? 'appDropdown' : 'normalDropdown'} dropDownElement`}>
                  <div className="dropDownBtn">
                    <i className="fa fa-bars fa-2x" onClick={() => { toggleDropDown(); }}></i>
                  </div>
                  <div className={`${dropdownState ? 'showDropDown' : ''} dropDown`}>
                    {dropDownLinks(userLoggedIn, resetDropDown)}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
};

const mapStateToProps = createStructuredSelector({
  dropdownState: selectDropDownState,
});

const mapDispatch = dispatch => ({
  toggleDropDown: () => dispatch(toggleDropDown()),
  resetDropDown: () => dispatch(resetDropDown()),
});

export default connect(mapStateToProps, mapDispatch)(HeaderComponent);
