//react
import React from 'react';
//misc
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

const CKEditorTextArea = ({ data, onChange }) => (
  <CKEditor
    editor={ ClassicEditor }
    data={data}
    onReady={ editor => { } }
    onChange={onChange}
    onBlur={ ( event, editor ) => { } }
    onFocus={ ( event, editor ) => { } }
  />
);

export default CKEditorTextArea;
