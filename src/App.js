
import React from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';
//styles
import './font/style.css'
//redux
import { connect } from 'react-redux';
import { resetDropDown } from './redux/header/header.actions';
import { selectLoadingSpinnerState } from './redux/app/app.selectors';
import { createStructuredSelector } from 'reselect';
//pages
import Recipes from './pages/recipes/recipes.component';
import RecipeEdit from './pages/recipe-edit/recipe-edit.component';
import RecipeNew from './pages/recipe-new/recipe-new.component';
import RecipeView from './pages/recipe-view/recipe-view.component';
import Error from './pages/error/error.component';
import Profile from './pages/profile/profile.component';
import Home from './pages/home/home.component';
import Login from './pages/login/login.component';
import Signup from './pages/signup/signup.component';
import ForgotCredentials from './pages/forgot-credentials/forgot-credentials.component';
import ResetPassword from './pages/reset-password/reset-password.component';
//components
import HeaderComponent from './components/header/header.component';
import LoadingSpinner from './components/loading-spinner/loading-spinner.component';
import GuardedRoute  from './components/guarded-route/guarded-route.component';


function App(props) {
  const { resetDropDown, loadingSpinnerState } = props;
  const location = useLocation();
  const userLoggedIn = (localStorage.getItem('token') != null) && (localStorage.getItem('refresh_token') != null)

  document.body.className = (userLoggedIn ? "app-background" :
    ((location.pathname === "/") ? "home-page" : "")
  );

  resetDropDown();

  return (
    <React.Fragment>
      <HeaderComponent
        userLoggedIn={userLoggedIn}
      />
      {loadingSpinnerState &&
        <LoadingSpinner />
      }
      <Switch>
        <GuardedRoute path='/recipes' exact component={Recipes} />
        <GuardedRoute path='/recipes/:recipe_id/edit' exact component={RecipeEdit} />
        <GuardedRoute path='/recipes/new' exact component={RecipeNew} />
        <GuardedRoute path='/recipes/:recipe_id' exact component={RecipeView} />
        <Route exact path="/errors" component={Error} />
        <GuardedRoute path='/profile' exact component={Profile} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/forgot-credentials" component={ForgotCredentials} />
        <Route exact path="/reset-password" component={ResetPassword} />
        <Route exact path="/" component={Home} />
        <Route path="*" component={Error} />
      </Switch>
    </React.Fragment>
  )
}
const mapStateToProps = createStructuredSelector({
  loadingSpinnerState: selectLoadingSpinnerState,
});

const mapDispatch = dispatch => ({
  resetDropDown: () => dispatch(resetDropDown()),
});

export default connect(mapStateToProps, mapDispatch)(App);
